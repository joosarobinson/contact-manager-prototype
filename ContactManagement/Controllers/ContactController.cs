﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using ContactManagement.Business;
using ContactManagement.Entities;
using Kendo.Mvc.UI;
namespace ContactManagement.Controllers
{

    public class ContactController : Controller
    {

        private readonly IBaseContactManager _contactService;

        public ContactController(IBaseContactManager contactService)
        {
            this._contactService = contactService;
        }
        // GET: Contact
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetContact(int? BasePoolId)
        {
            IEnumerable<EntContactInfo> enContactInfo = _contactService.GetContact(BasePoolId);
            return View("_Contact", enContactInfo);
        }
    }
}