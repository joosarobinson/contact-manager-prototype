﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using ContactManagement.Business;
using ContactManagement.Entities;
using Kendo.Mvc.UI;
using System.Linq;

namespace ContactManagement.Controllers
{
    public class LoginController : Controller
    {
        private readonly IBaseContactManager _contactService;

        private readonly IBasePoolManager _poolService;

        public LoginController(IBaseContactManager contactService, IBasePoolManager poolService)
        {
            this._contactService = contactService;
            this._poolService = poolService;
        }
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Home()
        {
            IEnumerable<EntContactInfo> enContactInfo = _contactService.GetContact(null);
             return View("Home", enContactInfo);
        }


 
        public ActionResult GetContact(int? BasePoolId)
        {
            IEnumerable<EntContactInfo> enContactInfo = _contactService.GetContact(BasePoolId);
            return View("Home", enContactInfo);
        }

        public ActionResult SearchPool(string Name)
        {
            IEnumerable<EntBasePoolInfo> enpoolInfo = _poolService.GetBasePool(Name);
            return PartialView("_PanelBar", enpoolInfo);
        }

        public JsonResult SearchPoolName(string Name)
        {
            IEnumerable<EntBasePoolInfo> enpoolInfo = _poolService.GetBasePool(Name);
            return Json(enpoolInfo, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult RelatedPool(int? ContactId)
        {
            IEnumerable<EntBasePoolInfo> poolInfo = _poolService.GetRelatedPoolByContact(ContactId);
            return Json(poolInfo, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Pdf_Export_Save(string contentType, string base64, string fileName)
        {
            var fileContents = Convert.FromBase64String(base64);

            return File(fileContents, contentType, fileName);
        }

        public ActionResult Pdf_Export_Read([DataSourceRequest]DataSourceRequest request)
        {
            IEnumerable<EntBaseContactInfo> enContactInfo = _contactService.GetBaseContact();
            return Json(enContactInfo);
        }

        //public ActionResult Employee_Read([DataSourceRequest]DataSourceRequest request)
        //{
        //    IQueryable<Bhupendra_employees> Details = _dbContext.Bhupendra_employees;
        //    DataSourceResult result = Details.ToDataSourceResult(request, p => new EmployeeViewModel
        //    {
        //        id = p.id,
        //        name = p.name,
        //        gender = p.gender,
        //        designation = p.designation,
        //        department = p.Bhupendra_Dept.Dept_Description
        //    });
        //    return Json(result, JsonRequestBehavior.AllowGet);
        //}

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}