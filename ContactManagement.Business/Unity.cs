﻿using Microsoft.Practices.Unity;
using ContactManagement.Unity;
using System.ComponentModel.Composition;

namespace ContactManagement.Business
{
    [Export(typeof(IModule))]
    public  class Unity : IModule
    {
        public void Initialize(IModuleRegistrar registrar)
        {
            registrar.RegisterType<IBaseContactManager, BaseContactManager>();
        }



    }
}
