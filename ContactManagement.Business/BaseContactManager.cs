﻿using ContactManagement.Data.DataLayer;
using System.Collections.Generic;
using ContactManagement.Entities;
namespace ContactManagement.Business
{

    public interface IBaseContactManager
    {
        IEnumerable<EntBaseContactInfo> GetBaseContact();

        IEnumerable<EntContactInfo> GetContact(int? BasePoolId);

    }
    public class BaseContactManager : IBaseContactManager
    {

        BaseContactDataManager _contactRepository = null;
        //private readonly IUnitOfWork _unitOfWork;
        public BaseContactManager()
        {
            this._contactRepository = new BaseContactDataManager();
        }


        public IEnumerable<EntBaseContactInfo> GetBaseContact()
        {
           
            return _contactRepository.GetBaseContact();
        }

        public IEnumerable<EntContactInfo> GetContact(int? BasePoolId)
        {
            return _contactRepository.GetContact(BasePoolId);
        }
    }
}
