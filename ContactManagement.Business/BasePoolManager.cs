﻿using ContactManagement.Data.DataLayer;
using System.Collections.Generic;
using ContactManagement.Entities;
namespace ContactManagement.Business
{

    public interface IBasePoolManager
    {
        IEnumerable<EntBasePoolInfo> GetBasePool(string Name);

        IEnumerable<EntBasePoolInfo> GetRelatedPoolByContact(int? ContactId);

    }
    public class BasePoolManager : IBasePoolManager
    {
        BasePoolDataManager _pool = null;
        //private readonly IUnitOfWork _unitOfWork;
        public BasePoolManager()
        {
            this._pool = new BasePoolDataManager();
        }


        public IEnumerable<EntBasePoolInfo> GetBasePool(string Name)
        {

            return _pool.GetBasePool(Name);
        }

        public IEnumerable<EntBasePoolInfo> GetRelatedPoolByContact(int? ContactId)
        {

            return _pool.GetRelatedPoolByContact(ContactId);
        }
    }
}
