﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContactManagement.Entities
{
    public class EntBaseContactInfo
    {
        public int ID { get; set; }
        public string ContactType { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Title { get; set; }
        public string AcademicGradeAffix { get; set; }
        public string OriginAffix { get; set; }
        public string JobTitleAffix { get; set; }
        public string GenealogicalAffix { get; set; }
        public string Note { get; set; }
        public Nullable<System.DateTimeOffset> InactiveSince { get; set; }
        public string InactiveCause { get; set; }
    }

    public class EntBasePoolInfo
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }


    public class EntContactInfo
    {
        public string Name { get; set; }
        public string Street { get; set; }
        public string HouseNumber { get; set; }
        public string ZipCode { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public int ID { get; set; }
        public string Referenz_zu { get; set; }
        public string RefName { get; set; }
        public string GroupHeader { get; set; }
    }
}
