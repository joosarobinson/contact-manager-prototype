﻿using System.Collections.Generic;
using ContactManagement.Data.Repository;
using ContactManagement.Entities;
using AutoMapper;
namespace ContactManagement.Data.DataLayer
{
    public class BaseContactDataManager : IBaseContactDataManager
    {
        readonly DBFactory dbo;


        public BaseContactDataManager()
        {
            dbo = new DBFactory();
        }


        public IEnumerable<EntBaseContactInfo> GetBaseContact()
        {
            Mapper.CreateMap<BaseContactInfo, EntBaseContactInfo>();
            IEnumerable<BaseContactInfo> lst = dbo.Get().UspProc_GetBaseContact();
            IEnumerable<EntBaseContactInfo> lst1 = Mapper.Map<IEnumerable<EntBaseContactInfo>>(lst);
           return lst1;
        }


        public IEnumerable<EntContactInfo> GetContact(int? BasePoolId)
        {
            Mapper.CreateMap<ContactInfo, EntContactInfo>();
            IEnumerable<ContactInfo> lst = dbo.Get().UspProc_GetContact(BasePoolId);
            IEnumerable<EntContactInfo> lst1 = Mapper.Map<IEnumerable<EntContactInfo>>(lst);
            return lst1;
        }
        //private hsKontakt2_DevEntities _dataContext;
        //private readonly IDbSet<T> dbset;
        //protected BaseContactManager(IDBFactory databaseFactory)
        //{
        //    DatabaseFactory = databaseFactory;
        //    dbset = DataContext.Set<T>();
        //}

        //protected IDBFactory DatabaseFactory
        //{
        //    get; private set;
        //}

        //protected hsKontakt2_DevEntities DataContext
        //{
        //    get { return _dataContext ?? (_dataContext = DatabaseFactory.Get()); }
        //}
    }
}
