﻿using System.Collections.Generic;
using ContactManagement.Data.Repository;
using ContactManagement.Entities;
using AutoMapper;
namespace ContactManagement.Data.DataLayer
{
    public class BasePoolDataManager : IBasePoolDataManager
    {
        readonly DBFactory dbo;


        public BasePoolDataManager()
        {
            dbo = new DBFactory();
        }


        public IEnumerable<EntBasePoolInfo> GetBasePool(string Name)
        {
            Mapper.CreateMap<BasePoolInfo, EntBasePoolInfo>();
            IEnumerable<BasePoolInfo> lst = dbo.Get().UspProc_GetBasePool(Name);
            IEnumerable<EntBasePoolInfo> lst1 = Mapper.Map<IEnumerable<EntBasePoolInfo>>(lst);
            return lst1;
        }

        public IEnumerable<EntBasePoolInfo> GetRelatedPoolByContact(int? ContactId)
        {
            Mapper.CreateMap<BasePoolInfo, EntBasePoolInfo>();
            IEnumerable<BasePoolInfo> poollist = dbo.Get().UspProc_GetRelatedPoolByContactId(ContactId);
            IEnumerable<EntBasePoolInfo> entpoollist = Mapper.Map<IEnumerable<EntBasePoolInfo>>(poollist);
            return entpoollist;
        }
    }
}
