﻿using System;
using ContactManagement.Data.Repository;
namespace ContactManagement.Data.DataLayer
{
    public interface IDBFactory : IDisposable
    {
        hsKontakt2_DevEntities Get();
    }
}
