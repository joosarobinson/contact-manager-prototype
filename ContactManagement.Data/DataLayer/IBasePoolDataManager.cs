﻿using System.Collections.Generic;
using ContactManagement.Entities;
namespace ContactManagement.Data.DataLayer
{
    interface IBasePoolDataManager
    {
        IEnumerable<EntBasePoolInfo> GetBasePool(string Name);

        IEnumerable<EntBasePoolInfo> GetRelatedPoolByContact(int? ContactId);
    }
}
