﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ContactManagement.Data.Repository;
using System.Data.Entity;
namespace ContactManagement.Data.DataLayer
{
    public class DBFactory : Disposable, IDBFactory
    {

        public DBFactory()
        {
            Database.SetInitializer<hsKontakt2_DevEntities>(null);
        }
        private hsKontakt2_DevEntities dataContext;
        public hsKontakt2_DevEntities Get()
        {
            return dataContext ?? (dataContext = new hsKontakt2_DevEntities());
        }
        protected override void DisposeCore()
        {
            if (dataContext != null)
                dataContext.Dispose();
        }
    }
}
