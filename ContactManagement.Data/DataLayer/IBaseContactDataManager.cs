﻿using System.Collections.Generic;
using ContactManagement.Entities;
namespace ContactManagement.Data.DataLayer
{
    public interface IBaseContactDataManager
    {
        IEnumerable<EntBaseContactInfo> GetBaseContact();

        IEnumerable<EntContactInfo> GetContact(int? BasePoolId);
    }
}
